{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data analysis - Introduction for Gamma Spectroscopy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Content\n",
    "\n",
    "* [About this Notebook](#about)\n",
    "\n",
    "\n",
    "* [Read experimental data from file](#read)\n",
    "    * [HelpCode](#help1)\n",
    "    * [Read the calibrated background spectrum](#backg)\n",
    "    * [Plotting the data](#plot)\n",
    "    \n",
    "\n",
    "* [Fit of data](#fit)\n",
    "    * [Fitting Gaussians](#gaussian)\n",
    "    * [Fit a line](#line)\n",
    "\n",
    "\n",
    "* [Statistical analysis](#stat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## About this Notebook <a name=\"about\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The purpose of this _jupyter_ notebook is to introduce data analysis in the frame of gamma spectroscopy. The example programming language is _Python3_, but of course most coding languages can do the job properly. If you have never programmed before there are so many great tutorials available across the web. There even exist plenty _Open Online Courses_, e.g. https://www.coursera.org/learn/python. Please have a look around for the one that you like the best. However, note that you do not need to be an expert in Python to pass the lab. \n",
    "\n",
    "The data analysis can roughly be divided into four steps:\n",
    "1. Read experimental data from file.\n",
    "2. Fit Gaussians to peaks. \n",
    "3. Calibrate the detector response.\n",
    "4. Perform a statistical analysis (e.g. error propagation) and present results.\n",
    "\n",
    "A dedicated python library, i.e. a folder with already written code, located in `HelpCode`, have been implemented for the data analysis connected to the labs in FYSC12 Nuclear Physics. The folder comprises functions that support 1-3 of the above-mentioned steps.\n",
    "\n",
    "Full Python3 coding examples of how to perform the different steps of the data analysis is given below. Every example is finished with a template of how the `HelpCode`-folder can be used to perform the same calculations. \n",
    "\n",
    "**NOTE**: It is strongly recommended that you program your own functions instead of using the framework directly out of the book. As you will find out, there will come a point where the framework functionalities are not to your satisfaction, and then you need to code yourself. So, better get used to it right away :)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read experimental data from file <a name=\"read\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code segment exemplifies how to read in an experimental data file into a list container. For an introduction on how to read and write files see e.g. http://www.pythonforbeginners.com/files/reading-and-writing-files-in-python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "array_of_data = list()\n",
    "read_lines = 0\n",
    "total_data = 0\n",
    "start_read = False\n",
    "with open(\"test_data.Spe\") as file:\n",
    "    for j, line in enumerate(file):\n",
    "        #if j < 100:\n",
    "            #print(line.split())\n",
    "        if line.split()[0] == '$DATA:':\n",
    "            #print(line.split())\n",
    "            start_read = True\n",
    "        elif start_read and read_lines == 0:\n",
    "            #print(line.split())\n",
    "            total_data = int(line.split()[1])+1\n",
    "            #print(total_data)\n",
    "            read_lines = read_lines + 1\n",
    "            continue\n",
    "        elif start_read and line.split()[0] != '$ROI:':\n",
    "            #print(line.split())\n",
    "            array_of_data.append(int(line.split()[0]))\n",
    "        elif start_read:\n",
    "            break\n",
    "        \n",
    "print(\"Should read:\", total_data, \"lines. And read:\", len(array_of_data))\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `HelpCode` <a name=\"help1\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the `help_code` it is possible to perform conceptually the same operations through:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from HelpCode.MCA import *\n",
    "data = load_spectrum(\"test_data.Spe\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read the calibrated background spectrum <a name=\"backg\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The background spectrum that is to be analysed as a part of the lab is named `background_analysis.csv` and can be found in the current folder. This spectrum has been measured with another detector and is already calibrated. Read this spectrum with the `help_code` with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "background_data = load_calibrated_spectrum(\"Background.txt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the data <a name=\"plot\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is always good to visualise your data. This is how you can plot and visualise it in Python3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "# choose a backend for web applications; remove for stand-alone applications:\n",
    "matplotlib.use('Agg')\n",
    "# enable interactive notebook plots (\n",
    "# alternative: use 'inline' instead of 'notebook' for static images)\n",
    "%matplotlib notebook\n",
    "\n",
    "#The following line is the ONLY one needed in stand-alone applications!\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "# with the data read in with the first routine\n",
    "#plt.plot(array_of_data)\n",
    "# or with the help_code variable \"data\"\n",
    "plt.plot(data.x, data.y)\n",
    "\n",
    "plt.savefig(\"test_spectrum.png\") #This is how you save the figure\n",
    "#axis = plt.gca()\n",
    "#axis.plot(array_of_data)\n",
    "\n",
    "## Could be useful to see this in log scale..?\n",
    "#plt.yscale('log')\n",
    "#plt.ylim(ymin=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fit of data <a name=\"fit\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fitting functions can be made simply with the `scipy.optimize` module. The function `curve_fit` does the job for you and the [documentation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html) contains all the valuable information on how to use the function. It uses a method called least squares which you can read about in most course literature on statistics and for instance on [Wolfram Alpha](http://mathworld.wolfram.com/LeastSquaresFitting.html). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting Gaussian <a name=\"gaussian\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code shows how to use the function `curve_fit` to fit a peak in the data that was read in above (i.e. you will need to execute the above code section before this section will work).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.optimize import curve_fit\n",
    "\n",
    "def GaussFunc(x, A, mu, sigma):\n",
    "    return A*np.exp(-(x-mu)**2/(2.*sigma**2))\n",
    "\n",
    "#mu = np.asarray([3300, 3750])\n",
    "#E = np.asarray([1173.2, 1332.5])\n",
    "mu = 3300\n",
    "A = array_of_data[mu]\n",
    "sigma = 1\n",
    "guess = [A, mu, sigma]\n",
    "n = 50 #number of points on each side to include in fit\n",
    "\n",
    "x = np.asarray(range(len(array_of_data)))\n",
    "y = np.asarray(array_of_data)\n",
    "\n",
    "estimates, covar_matrix = curve_fit(GaussFunc,\n",
    "                                    x[mu-n:mu+n],\n",
    "                                    y[mu-n:mu+n],\n",
    "                                    p0=guess)\n",
    "\n",
    "print(\"Estimates of (A mu sigma) = (\", estimates[0], estimates[1], estimates[2], \")\\n\")\n",
    "\n",
    "print(\"Covariance matrix = \\n\", covar_matrix, \"\\n\")\n",
    "\n",
    "print(\"Uncertainties in the estimated parameters: \\n[ sigma^2(A) sigma^2(mu), sigma^2(sigma) ] = \\n[\", covar_matrix[0][0], covar_matrix[1][1], covar_matrix[2][2], \"]\" )\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(x[mu-n:mu+n],y[mu-n:mu+n], linestyle=\"\", marker=\"*\")\n",
    "plt.plot(x[mu-n:mu+n], GaussFunc(x[mu-n:mu+n], estimates[0], estimates[1], estimates[2]))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### HelpCode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the `HelpCode`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from HelpCode.fithelpers import *\n",
    "\n",
    "gauss = fit_gaussian_at_idx(x, y, mu, npoints=n)\n",
    "print(\"Estimates of (A mu sigma) = (\", gauss.A, gauss.mu, gauss.sigma, \")\\n\")\n",
    "\n",
    "print(\"Covariance matrix = \\n\", gauss.covar_matrix, \"\\n\")\n",
    "\n",
    "print(\"Uncertainties in the estimated parameters: \\n[ sigma^2(A) sigma^2(mu), sigma^2(sigma) ] = \\n[\", gauss.covar_matrix[0][0], gauss.covar_matrix[1][1], gauss.covar_matrix[2][2], \"]\" )\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(data.x[mu-n:mu+n], data.y[mu-n:mu+n], linestyle=\"\", marker=\"*\")\n",
    "plt.plot(data.x[mu-n:mu+n], gauss.value(data.x)[mu-n:mu+n])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit a line <a name=\"line\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.asarray([1,3,5,7])\n",
    "y = np.asarray([1.3, 2.1, 2.9, 4.2])\n",
    "#If you are more or less uncertain about your y-values this can be used in the fit by including the following line.\n",
    "sigmay = np.asarray([0.5, 0.3, 0.1, 0.2])\n",
    "\n",
    "guess = [2, 1]\n",
    "\n",
    "def LineFunc(x, k, m):\n",
    "    return k*x+m\n",
    "\n",
    "estimates, covar_matrix = curve_fit(LineFunc,\n",
    "                                    x,\n",
    "                                    y,\n",
    "                                    p0 = guess,\n",
    "                                    sigma = sigmay)\n",
    "\n",
    "print(\"Estimates of (k m) = (\", estimates[0], estimates[1], \")\\n\")\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(x,y, linestyle=\"\", marker=\"*\")\n",
    "plt.plot(x, LineFunc(x, estimates[0], estimates[1]))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Statistical analysis <a name=\"stat\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Background theory and instructions on how to perform statistical analysis on experimental data, with error propagation, can be found in the document `error_analysis.pdf`), but of course also easily through a google search."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
